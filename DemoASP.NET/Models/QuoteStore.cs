﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoASP.NET.Models
{
    public static class QuoteStore
    {
        public static List<Quote> Quotes = new List<Quote>() 
        {
            new Quote
            {
                Author = "Anonymous",
                NewQuote = "The cost of not following your heart is spending the rest of your life wishing you had!!"
            },
            
            new Quote
            {
                Author = "Anonymous",
                NewQuote = "Na mumu dey sleep for night!!"
            },

        };

        public static List<Quote> GetQuotes() => Quotes;

    }
}
